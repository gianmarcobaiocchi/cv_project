import json
import face_recognition
import cv2
import numpy as np
import threading


class ImageRecognizer:
    # Fields
    __known_faces = []
    __known_names = []
    __file_path = "registered_user.json"
    __name_string = "Write your name: "
    __username = ""
    __data_lock = threading.Lock()

    # Builder
    def __init__(self):
        self.__known_faces, self.__known_names = self.__generate_images_array()

    def __generate_images_array(self):
        known_face_encodings = []
        known_face_names = []
        json_file = open(self.__file_path)
        users = json.load(json_file)
        json_file.close()
        for user in users:
            known_face_encodings.append(np.asarray(user["encoding"]))
            known_face_names.append(user["name"])

        return known_face_encodings, known_face_names

    def __save_face(self, face_encoding, name_id):
        # Creates or updates a JSON file for all saved users faces
        res = []
        json_file = open(self.__file_path, "w")
        self.__known_faces.append(face_encoding[0])
        self.__known_names.append(name_id)
        for user in zip(np.arange(len(self.__known_names), dtype=int), self.__known_names, self.__known_faces):
            res.append({"Id": int(user[0]), "name": user[1], "encoding": user[2].tolist()})
        json.dump(res, json_file)
        json_file.close()

    def __sign_in(self, face_encoding):
        with self.__data_lock:
            self.__username = input(self.__name_string)
            self.__save_face(face_encoding, self.__username)
        print("Utente registrato!")

    def __thread_sign_in(self, face_encoding):
        thread = threading.Thread(target=self.__sign_in, args=(face_encoding, ))
        thread.start()

    def recognize_face(self, frame, face_bounding_box):
        """
        Tries to recognize the user's face.
        :param frame: frame in which is present the user's face in BGR encoding.
        :param face_bounding_box: user's face bounding box (x, y, w, h).
        :return: the user's name.
        """
        small_frame = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
        face_encoding = face_recognition.face_encodings(small_frame, list([face_bounding_box]))

        if self.__data_lock.locked():
            return ""

        if len(self.__known_faces) != 0:
            matches = face_recognition.compare_faces(self.__known_faces, face_encoding[0])
            face_distances = face_recognition.face_distance(self.__known_faces, face_encoding[0])
            best_match_index = np.argmin(face_distances)
            if matches[best_match_index]:
                self.__username = self.__known_names[best_match_index]
                return self.__username

        # User is not recognized
        self.__thread_sign_in(face_encoding)

        return ""
