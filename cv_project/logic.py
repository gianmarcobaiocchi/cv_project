import cv2
import numpy as np
import dlib
import math

# ----------------------------------------------------------------------------------------------------------------------
# Variables
# ----------------------------------------------------------------------------------------------------------------------

detectFace = dlib.get_frontal_face_detector()  # Face detector object
predictor = dlib.shape_predictor("predictor/shape_predictor_68_face_landmarks.dat")  # Landmarks detector
LANDMARKS_N = 68
CENTER_INDEX = 2
BLACK_VALUE = 0
WHITE_VALUE = 255
BLUR_MASK_SIZE = 5
BLUR_SIGMA_X = 0    # Gaussian kernel standard deviation in X direction
THRESHOLD_VALUE = 60
SPATIAL_MASK_SIZE = 3


# ----------------------------------------------------------------------------------------------------------------------
# Public functions
# ----------------------------------------------------------------------------------------------------------------------

def detect_faces(image):
    """
    Function that takes an image as input, and returns a list of faces, where the first one is the nearest to the cam
    :param image: input image
    """
    faces = detectFace(__get_gray_image(image))
    ret = np.array(faces)
    if len(ret) > 1:
        for i in np.arange(1, len(ret)):
            if __get_face_area(ret[i]) > __get_face_area(ret[0]):
                ret[i], ret[0] = ret[0], ret[i]
    return ret


def face_landmarks_detector(image, face):
    """
    Function that takes as input an image and a face, and returns a list of reference points of the face
    :param image: input image
    :param face: input face
    """
    reference_points = []
    landmarks = predictor(__get_gray_image(image), face)  # Landmarks predictor
    for i in range(0, LANDMARKS_N):
        point = (landmarks.part(i).x, landmarks.part(i).y)  # Get x,y coordinates of each landmark
        reference_points.append(point)
    return reference_points


def is_looking_at_cam(image, eye):
    """
    Function that takes as input an image and the eye, and returns if the eye is looking at the cam
    :param image: input image
    :param eye: input eye
    """

    image_grey = __get_gray_image(image)

    # Getting the max and min points of eye inorder to crop the eye from eye_image
    max_x = (max(eye, key=lambda item: item[0]))[0]
    min_x = (min(eye, key=lambda item: item[0]))[0]
    max_y = (max(eye, key=lambda item: item[1]))[1]
    min_y = (min(eye, key=lambda item: item[1]))[1]
    cropped_eye = image_grey[min_y:max_y, min_x:max_x]  # Cropping the eye
    height, width = cropped_eye.shape

    # Applying an averaging spatial filtering mask to the eye
    if (height > SPATIAL_MASK_SIZE * 2) and (width > SPATIAL_MASK_SIZE * 2):
        margin = int((SPATIAL_MASK_SIZE - 1) / 2)
        cropped_eye = __averaging_filtering(cropped_eye, SPATIAL_MASK_SIZE)  # Applying the spatial filter to the image
        cropped_eye = cropped_eye[margin: (height - margin), margin: (width - margin)]  # Cutting contour created
        # after filtering
        height, width = cropped_eye.shape  # Updating the height and width value, after the new cropping

    # Applying a gaussian blur mask to the eye
    if (height > BLUR_MASK_SIZE) and (width > BLUR_MASK_SIZE):
        cropped_eye_blurred = cv2.GaussianBlur(cropped_eye, (BLUR_MASK_SIZE, BLUR_MASK_SIZE), BLUR_SIGMA_X)
        ret, threshold_eye = cv2.threshold(cropped_eye_blurred, THRESHOLD_VALUE, 255,
                                           cv2.THRESH_BINARY_INV + cv2.THRESH_OTSU)
    else:
        ret, threshold_eye = cv2.threshold(cropped_eye, THRESHOLD_VALUE, 255, cv2.THRESH_BINARY_INV + cv2.THRESH_OTSU)

    # Applying erosion first, then dilation to the eye using a square and dynamic-sized pattern
    p_size = int(height / 4) + 1  # Getting dynamically the size for the pattern
    threshold_eye_e = cv2.erode(threshold_eye, cv2.getStructuringElement(cv2.MORPH_ELLIPSE, (p_size, p_size)))
    threshold_eye_d = cv2.dilate(threshold_eye_e, cv2.getStructuringElement(cv2.MORPH_ELLIPSE, (p_size, p_size)))

    # Finding the largest component in the image (that should be the eye), filling it
    threshold_eye = __largest_component_mask(threshold_eye_d)

    # Invert the binary image values
    threshold_eye = cv2.bitwise_not(threshold_eye)

    # Dividing the width of the eye into three parts
    div_part = int(width / 3)
    right_part = threshold_eye[0:height, 0:div_part]
    center_part = threshold_eye[0:height, div_part:div_part * 2]
    left_part = threshold_eye[0:height, div_part * 2:width]

    # Counting black pixel in each part
    right_black_px = np.sum(right_part == 0)
    center_black_px = np.sum(center_part == 0)
    left_black_px = np.sum(left_part == 0)
    values = [right_black_px, left_black_px, center_black_px]

    # Checking if the pupil of the eye is in the center of the image
    if values.index(max(values)) == CENTER_INDEX:
        for i in range(div_part):
            column = threshold_eye[0:height, div_part + i]
            if np.sum(column == WHITE_VALUE) < 2:
                return True
    return False


# ----------------------------------------------------------------------------------------------------------------------
# Private functions
# ----------------------------------------------------------------------------------------------------------------------

def __midpoint(cord1, cord2):
    x1, y1 = cord1
    x2, y2 = cord2
    return int((x1 + x2) / 2), int((y2 + y1) / 2)


def __euclidean_distance(cord1, cord2):
    x1, y1 = cord1
    x2, y2 = cord2
    return math.sqrt(pow((x2 - x1), 2) + pow((y2 - y1), 2))


def __get_gray_image(image):
    return cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)


def __get_face_area(face):
    return (face.right() - face.left()) * (face.bottom() - face.top())


def __averaging_filtering(image, mask_size):
    """
    Function that apply a averaging spatial filter over the input image .
    :param image: input binary image
    :param mask_size: size of the side of the mask. It must be an odd number
    """
    return cv2.blur(image, (mask_size, mask_size))


def __largest_component_mask(image):
    """
    Function that finds the largest component in a binary image and returns the component as a mask.
    :param image: input binary image
    """
    max_area = [0, 0]  # (contour_index, area)
    contours = cv2.findContours(image, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_NONE)[0]

    for i, contour in enumerate(contours):
        contour_area = cv2.moments(contour)['m00']
        if contour_area > max_area[1]:  # Looking for the contour with the largest area
            max_area[0] = i
            max_area[1] = contour_area

    labeled_image = np.zeros(image.shape, dtype=np.uint8)
    cv2.drawContours(labeled_image, contours, max_area[0], color=255, thickness=-1)
    return labeled_image
